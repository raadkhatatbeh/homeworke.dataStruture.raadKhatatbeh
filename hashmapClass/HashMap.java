package hashmapClass;


import java.util.ArrayList;
import java.util.LinkedList;

public class HashMap<K,V> {

    public class Node{
        // evry node contian key and value
        K key;
        V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
    private int n ; // number of node
    private int N ;// number of buckets
    private LinkedList<Node> buckets[] ;

    @SuppressWarnings("unchecked")
    public HashMap(){
        this.N = 4;
        this.buckets = new LinkedList[N];
       for (int i = 0; i<4; i++)
           buckets[i] = new LinkedList();
    }
    public void put(K key, V value){
         int bi = hashFunction(key);
         int di = searchInLl(key, bi);

         if (di == -1){
             buckets[bi].add(new Node(key,value));
             n++;
         }else {
             Node node = buckets[bi].get(di);
            node.value = value;
         }
         double lamda = (double)n/N;
         if (lamda > 2.0)
             reHash();
    }
    private int searchInLl(K key, int bi) {

        LinkedList<Node> search = buckets[bi];
        for (int i = 0; i<search.size();i++){
            if (key == search.get(i).key){
                return i ;
            }
        }
        return -1;
    }
    private int hashFunction(K key) {
        int bi = key.hashCode();
        return Math.abs(bi)% N;
    }

    private void reHash() {
        LinkedList<Node> oldBuckets[] = buckets;
        buckets = new LinkedList[N*2];

        for (int i = 0;i<N*2;i++){
            buckets[i] = new LinkedList<>();
        }
        for (int i = 0; i<oldBuckets.length; i++){
            LinkedList<Node> l1 = oldBuckets[i];
            for (int j = 0; j<l1.size(); i++){
                Node node = l1.get(i);
                put(node.key, node.value);
            }
        }
     }


    public V get(K key){
        int bi = hashFunction(key);
        int di = searchInLl(key, bi);
        if (di == -1) {
           return null;
        }else {
            Node node = buckets[bi].get(di);
            return node.value;
        }
    }
    public boolean contianKey(K key){
        int bi = hashFunction(key);
        int di = searchInLl(key, bi);
        if (di == -1)
            return false;
           return true;

        }


    public V removeKey(K key){
        int bi = hashFunction(key);
        int di = searchInLl(key, bi);

        if (di == -1) {
            return null;
        }else {
            Node node = buckets[bi].remove(di);
            n--;
            return node.value;

        }
    }

    public boolean isEmpty(){
        return n==0;
    }
    public ArrayList<K> keySet () {
        ArrayList<K> arrayList = new ArrayList<>();
        for (int i = 0; i < buckets.length; i++) {
            LinkedList<Node> l1 = buckets[i];
            for (int j = 0; j < l1.size(); j++) {
                Node node = l1.get(j);
                arrayList.add(node.key);
            }
        }
        return arrayList;

    }


public static void main(String args[]) {

        HashMap<String,Integer> map = new HashMap<>();
        map.put("raad",1);
        map.put("yazen",17);
        map.put("mohammed",6);
        map.put("fffff",2);

        ArrayList<String> m = map.keySet();
        for (int i = 0; i<m.size();i++){
            System.out.println(m.get(i)+" = "+ map.get(m.get(i)));
        }


}

}
