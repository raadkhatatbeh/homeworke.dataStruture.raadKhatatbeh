package array;

import java.util.HashMap;
import static java.lang.System.*;

public class ArrayNumber {

    public static  void main(String args[]){
        int a[] = {1,2,2,1,3,3,1,1,3,4,4};
        int n = a.length;
        int result = 0;
        for(int i = 0; i<n;i++){
            result = result^a[i];
        }
        out.println(result);

        }

    public static  HashMap<Integer,Integer> findDeplicatedNumber(int [] arry){
        HashMap<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i<arry.length;i++){
            if (map.containsKey(arry[i]))
            {
                map.put(arry[i],map.get(arry[i])+1);
            }
            else {
                map.put(arry[i],1);
            }

        }
        return map;
    }


}


