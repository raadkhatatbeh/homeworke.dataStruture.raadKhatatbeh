package stackofarray;


public class Main {

    public static void main(String args[]){

        Stack stack = new Stack();

        String str = "{([])}";
        String str1 = "{([)}";
        for (int i = 0; i<str1.length();i++) {
            char c = str1.charAt(i);
            if (c == '{' || c == '[' || c == '(' || c == '<') {
                stack.push(c);

            } else if (!stack.isEmpty() && ((c == '}' && stack.peek() == '{')
                    || (c == ')' && stack.peek() == '('))
                    || (c == '>' && stack.peek() == '<')
                    || (c == ']' && stack.peek() == '[')) {
                stack.pop();
            } else stack.push(c);
        }
        if (stack.isEmpty())
            System.out.println("balance ");
        else System.out.println("not balance");

    }
}
