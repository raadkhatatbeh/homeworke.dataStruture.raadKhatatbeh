package stackofarray;
public class Stack {
    char [] stack = new char[1];
    int top = 0;

    public void push(char c ){
        expande();
        stack[top] = c;
        top++;
    }
    public void top(){
        System.out.println(stack[--top]);
    }
    public char peek() {
        if (top > 0)
            return stack[top-1];
        return 'n';
    }

    public void pop(){
        top = top-1;
        stack[top] = 0;
        shrink();
    }
    public void show(){
        for (int i = stack.length-1; i>=0;i--){
            System.out.println(stack[i]);
        }
    }
    public boolean isEmpty(){
        if (this.top <=0)
            return true;
        return false;
    }

    private void expande() {
        if (top<stack.length)
            return;
        char [] newStack = new char[stack.length+1];
        for (int i = 0; i<stack.length;i++){
            newStack[i] = stack[i];
        }
        stack = newStack;
    }

    public void shrink(){
        char[] newStack = new char[stack.length-1];
        for (int i= 0; i<stack.length-1;i++){
            newStack[i] = stack[i];
        }
        stack = newStack;
    }

}
