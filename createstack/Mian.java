package createstack;

import hashmapClass.HashMap;

public class Mian {
/*
    public static void main(String args[]){

        Stack stack = new Stack();
        String str1 = "{([])}";
        String str2 = "}}}{{{";

        for (int i = 0; i<str2.length();i++){
            char c = str2.charAt(i);
            if (c == '{' || c=='[' || c=='('|| c=='<'){
                stack.push(c);
            }
            else if (!stack.isEmpty() &&((c == '}' && stack.peek() == '{')
                       ||(c == ')' && stack.peek() == '(') )
                       || (c == '>' && stack.peek() == '<')
                       || (c == ']' && stack.peek() == '[')) {
                stack.pop();
            }
            else stack.push(c);
        }
        if (stack.isEmpty())
            System.out.println("balance ");
            else
                System.out.println("not balance");
    }*/

    public static void main(String[] args) {

        System.out.println(isValid("}}{{"));
    }

    public static boolean isValid(String s) {

        hashmapClass.HashMap<Character, Character> stringMap = new HashMap<>();
        Stack stack = new Stack();

        stringMap.put('(', ')');
        stringMap.put('{', '}');
        stringMap.put('[', ']');

        for(int i = 0; i < s.length(); i++){
            char bracket = s.charAt(i);

            if (stack.isEmpty() && !stringMap.contianKey(bracket))
                return false;
            if (stack.isEmpty()) {
                stack.push(bracket);
                continue;
            }
            if(stringMap.get(stack.peek()) == bracket)
                stack.pop();
            else {
                if(stringMap.contianKey(bracket))
                    stack.push(bracket);
                else
                    return false;
            }
        }

        if (stack.isEmpty())
            return true;
        else
            return false;

    }






}
