package array;

public class Main {
    public static void main(String[] args) {
        int arr[] = new int[]{2, 3,1,5,7,9,3};
        int n = arr.length;

        System.out.println(repeatedNumber(arr));

    }
    public static int repeatedNumber(int array[]) {
        if (array.length <= 1) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[Math.abs(array[i])] > 0) {
                array[Math.abs(array[i])] = -1 * array[Math.abs(array[i])];
            } else {
                return Math.abs(array[i]);
            }
        }
        return 0;
    }
}

